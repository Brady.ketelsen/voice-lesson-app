from django.contrib import admin
from lessons.models import Lesson, Payment


# Register your models here.
@admin.register(Lesson)
class LessonAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "id",
        "date",
        "paid",
        "student",
    )


@admin.register(Payment)
class PaymentAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "id",
        "date",
        "amount",
        "student",
    )

from django import forms
from lessons.models import Lesson, Payment
from django.contrib.admin.widgets import AdminDateWidget


class LessonForm(forms.ModelForm):
    class Meta:
        model = Lesson
        fields = (
            "name",
            "date",
            "time",
            "paid",
            "lit",
            "techniques",
            "homework",
            "student",
        )
        widgets = {
            "date": forms.SelectDateWidget(),
            "time": forms.TimeInput(format="%H:%M"),
            "time": forms.TextInput(attrs={"placeholder": "24:00"}),
        }


class PaymentForm(forms.ModelForm):
    class Meta:
        model = Payment
        fields = (
            "name",
            "date",
            "amount",
            "student",
        )
        widgets = {
            "date": forms.SelectDateWidget(),
        }

from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class Lesson(models.Model):
    name = models.CharField(max_length=150)
    date = models.DateField()
    time = models.TimeField(null=True)
    paid = models.BooleanField(default=False)
    lit = models.TextField(blank=True, null=True)
    techniques = models.TextField(blank=True, null=True)
    homework = models.TextField(blank=True, null=True)
    student = models.ForeignKey(
        User, related_name="student", on_delete=models.CASCADE, null=True, blank=True
    )


class Payment(models.Model):
    name = models.CharField(max_length=150)
    date = models.DateField()
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    student = models.ForeignKey(
        User, related_name="payer", on_delete=models.CASCADE, null=True, blank=True
    )

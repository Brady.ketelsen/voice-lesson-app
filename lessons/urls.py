from django.urls import path
from lessons.views import (
    home_page,
    lessons_list,
    lessons_detail,
    my_lessons_list,
    create_lesson,
    create_payment,
    edit_payment,
    edit_lesson,
    my_lessons_list,
    delete_lesson,
    delete_payment,
)

urlpatterns = (
    path("", home_page, name="home_page"),
    path("lessons_list/", lessons_list, name="lessons_list"),
    path("my_lessons_list/", my_lessons_list, name="my_lessons_list"),
    path("detail/<int:id>/", lessons_detail, name="lessons_detail"),
    path("create_lesson/", create_lesson, name="create_lesson"),
    path("edit_lesson/<int:id>/", edit_lesson, name="edit_lesson"),
    path("create_payment/", create_payment, name="create_payment"),
    path("edit_payment/<int:id>/", edit_payment, name="edit_payment"),
    path("delete_lesson/<int:id>/", delete_lesson, name="delete_lesson"),
    path("delete_payment/<int:id>/", delete_payment, name="delete_payment"),
)

from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse
from lessons.models import Lesson, Payment
from lessons.forms import LessonForm, PaymentForm

# Create your views here.


def home_page(request):
    return render(request, "lessons/home.html")


def lessons_list(request):
    lessons = Lesson.objects.all().order_by("-date").values()
    payments = Payment.objects.all().order_by("-date").values()
    context = {
        "lessons": lessons,
        "payments": payments,
    }
    return render(request, "lessons/lessons_list.html", context)


def my_lessons_list(request):
    lessons = Lesson.objects.filter(student=request.user).order_by("-date").values()
    payments = Payment.objects.filter(student=request.user).order_by("-date").values()
    unpaid = 0
    for lesson in lessons:
        if lesson["paid"] == False:
            unpaid += 1
    owed = unpaid * 60
    context = {
        "lessons": lessons,
        "payments": payments,
        "unpaid": unpaid,
        "owed": owed,
    }
    return render(request, "lessons/my_lessons_list.html", context)


def lessons_detail(request, id):
    lesson = get_object_or_404(Lesson, id=id)
    context = {
        "lesson": lesson,
    }
    return render(request, "lessons/lessons_detail.html", context)


def create_lesson(request):
    if request.method == "POST":
        form = LessonForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("lessons_list")
    else:
        form = LessonForm
    context = {
        "form": form,
    }
    return render(request, "lessons/create_lesson.html", context)


def create_payment(request):
    if request.method == "POST":
        form = PaymentForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("lessons_list")
    else:
        form = PaymentForm
    context = {
        "form": form,
    }
    return render(request, "lessons/create_payment.html", context)


def edit_payment(request, id):
    payment = get_object_or_404(Payment, id=id)
    if request.method == "POST":
        form = PaymentForm(request.POST, instance=payment)
        if form.is_valid():
            form.save()
            return redirect("lessons_list")
    else:
        form = PaymentForm(instance=payment)
    context = {
        "form": form,
        "payment": payment,
    }
    return render(request, "lessons/edit_payment.html", context)


def edit_lesson(request, id):
    lesson = get_object_or_404(Lesson, id=id)
    if request.method == "POST":
        form = LessonForm(request.POST, instance=lesson)
        if form.is_valid():
            form.save()
            return redirect("lessons_list")
    else:
        form = LessonForm(instance=lesson)
    context = {
        "form": form,
        "lesson": lesson,
    }
    return render(request, "lessons/edit_lesson.html", context)


def delete_lesson(request, id):
    lesson = Lesson.objects.get(id=id)
    if request.method == "POST":
        lesson.delete()
        return redirect("lessons_list")
    return render(request, "lessons/delete_lesson.html")


def delete_payment(request, id):
    payment = Payment.objects.get(id=id)
    if request.method == "POST":
        payment.delete()
        return redirect("lessons_list")
    return render(request, "lessons/delete_payment.html")
